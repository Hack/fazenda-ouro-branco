		</div>
	</div>

	<footer>
		<div class="placa">
			<img src="<?php echo base_url('public/img/placa.png');?>">
			<div class="icone-sol">
				<i class="fa fa-sun-o"></i>
				<a href="javascript:void(0);" onclick="Geral.FundoHome('dia');"></a>
			</div>
			<div class="icone-lua">
				<i class="fa fa-moon-o"></i>
				<a href="javascript:void(0);" onclick="Geral.FundoHome('noite');"></a>
			</div>
			
			<a href="" target="_blank" class="icone-facebook"></a>

		</div>
		<div class="boi">
			<img src="<?php echo base_url('public/img/gado.png');?>" width="215" height="185">
		</div>
		<div class="direitos">
			<p><?php echo date("Y");?> fazendaourobranco - Todos os direitos reservados</p> 
		</div>

		<div class="desenvolvedor">
			<p>Desenvolvido por: <a href="http://matheushack.com.br" target="_blank">Matheus Hack</a></p>
		</div>
	</footer>
</body>
</html>