<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title>Fazenda Ouro Branco</title>

	<link rel="stylesheet" href="<?php echo base_url('public/css/geral.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('public/css/jquery-ui.css');?>">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url('public/css/jquery.jcarousel.css');?>">

	<script type="text/javascript">var base_url = "<?php echo base_url();?>";</script>
	<script type="text/javascript" src="<?php echo base_url('public/js/jquery.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('public/js/jquery-ui.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('public/js/jquery.jcarousel.js');?>"></script>	
	<script type="text/javascript" src="<?php echo base_url('public/js/fazenda.js');?>"></script>
</head>
<body style="background-color:<?php echo $fundo;?>">
	<div class="box-centralizado">
		<header>
			<div class="logo">
				<h1>Fazenda Ouro Branco</h1>
				<a href="<?php echo base_urL();?>"><img src="<?php echo base_url('public/img/logo.png');?>" alt="Fazenda Ouro Branco"></a>
			</div>

			<nav>
				<a href="<?php echo base_url();?>" <?php echo ($menu_ativo == "home" ? "class='ativo'" : "");?>>Home</a>
				<a href="<?php echo base_url('a-fazenda');?>" <?php echo ($menu_ativo == "a-fazenda" ? "class='ativo'" : "");?>>A Fazenda</a>
				<a href="<?php echo base_url('criacao-de-gados');?>" <?php echo ($menu_ativo == "criacao-de-gados" ? "class='ativo'" : "");?>>Criação de Gados</a>
				<a href="<?php echo base_url('gados');?>" <?php echo ($menu_ativo == "gados" ? "class='ativo'" : "");?>>Gados</a>
				<a href="<?php echo base_url('cadastro');?>" <?php echo ($menu_ativo == "cadastro" ? "class='ativo'" : "");?>>Cadastro</a>
				<a href="<?php echo base_url('contato');?>" <?php echo ($menu_ativo == "contato" ? "class='ativo'" : "");?>>Contato</a>
				<a href="<?php echo ADMIN;?>" target="_blank">Login</a>
			</nav>
		</header>

		<div class="box-conteudo">