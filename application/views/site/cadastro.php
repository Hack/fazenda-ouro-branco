<h2>Cadastro</h2>
<p>
	Preencha o formulário abaixo para se cadastrar em nosso site.
</p>
<div id="cadastro">
	<div id="informacoes-cadastro">
		<img src="<?php echo base_url('public/img/cadastro.jpg');?>">
		<p class="centralizado"><strong>Fazenda Ouro Branco</strong></p>
		<p>
			Telefone: (XX)XXXX-XXXX <br>
			E-mail: contato@fazendaourobranco.com.br <br>
			Endereço: Rua xxxx, São Paulo-SP
		</p>
	</div>
	<div id="form-cadastro">
		<form name="formCadastro" action="<?php echo base_url('site/salvar_cadastro');?>" method="post">
			<section>
				<label class="descricao-input">
					<span class="asterisco">*</span> Nome
				</label>
				<label class="input-text">
					<input type="text" name="nome" maxlength="50">
				</label>
			</section>

			<section>
				<label class="descricao-input">
					<span class="asterisco">*</span> E-mail
				</label>
				<label class="input-text">
					<input type="text" name="email" maxlength="150">
				</label>
			</section>			

			<section>
				<label class="descricao-input">
					<span class="asterisco">*</span> Endereço
				</label>
				<label class="input-text">
					<input type="text" name="endereco" maxlength="150">
				</label>
			</section>

			<section>
				<label class="descricao-input">
					Complemento
				</label>
				<label class="input-text">
					<input type="text" name="complemetno" maxlength="150">
				</label>
			</section>	

			<section>
				<label class="descricao-input">
					<span class="asterisco">*</span> Estado
				</label>
				<label class="input-select">
					<select name="estado">
						<option value="" selected="">Selecione um estado</option>
						<option value="AC">AC</option>	
						<option value="AL">AL</option>	
						<option value="AP">AP</option>	
						<option value="AM">AM</option>	
						<option value="BA">BA</option>	
						<option value="CE">CE</option>	
						<option value="DF">DF</option>	
						<option value="ES">ES</option>	
						<option value="GO">GO</option>	
						<option value="MA">MA</option>	
						<option value="MT">MT</option>	
						<option value="MS">MS</option>	
						<option value="MG">MG</option>	
						<option value="PA">PA</option>	
						<option value="PB">PB</option>	
						<option value="PR">PR</option>	
						<option value="PE">PE</option>	
						<option value="PI">PI</option>	
						<option value="RJ">RJ</option>	
						<option value="RN">RN</option>	
						<option value="RS">RS</option>	
						<option value="RO">RO</option>	
						<option value="RR">RR</option>	
						<option value="SC">SC</option>	
						<option value="SP">SP</option>	
						<option value="SE">SE</option>	
						<option value="TO">TO</option>	
					</select>
				</label>
			</section>

			<section>
				<label class="descricao-input">
					<span class="asterisco">*</span> Telefone
				</label>
				<label class="input-text">
					<input type="text" name="telefone" class="telefone">
				</label>
			</section>

			<section>
				<label class="descricao-input">
					<span class="asterisco">*</span> Senha
				</label>
				<label class="input-password">
					<input type="password" name="telefone" maxlength="6">
				</label>
			</section>			

			<button id="btn-cadastrar">Cadastrar</button>
		</form>
	</div>
</div>