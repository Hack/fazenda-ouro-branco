<div class="jcarousel-wrapper">
    <div class="jcarousel">
        <ul>
            <li><img src="<?php echo base_url('public/img/banners/banner-1.jpg');?>" width="850" height="400" alt=""></li>
            <li><img src="<?php echo base_url('public/img/banners/banner-2.jpg');?>" width="850" height="400" alt=""></li>
            <li><img src="<?php echo base_url('public/img/banners/banner-3.jpg');?>" width="850" height="400" alt=""></li>
        </ul>
    </div>

    <a href="javascript:void(0);" class="jcarousel-control-prev">&lsaquo;</a>
    <a href="javascript:void(0);" class="jcarousel-control-next">&rsaquo;</a>
    
    <p class="jcarousel-pagination">
        
    </p>
</div>

<div class="box-esquerdo"></div>

<div class="box-direito"></div>

<script type="text/javascript">
	$(document).ready(function(){
		Home.init();
	});
</script>