<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<html lang="en">

<head>
	<meta charset="utf-8">
    <title>Fazenda Ouro Branco</title>
	<link rel="stylesheet" href="<?php echo base_url('public/css/pagina-entrada/style.css');?>" type="text/css" charset="utf-8" />
	<link rel="stylesheet" href="<?php echo base_url('public/css/pagina-entrada/ie.css');?>" type="text/css" charset="utf-8" />
	<script language="Javascript" type="text/javascript" src="<?php echo base_url('public/js/pagina-entrada/jquery-1.4.4.js');?>"></script>
	<script language="Javascript" type="text/javascript" src="<?php echo base_url('public/js/pagina-entrada/jquery.lwtCountdown-1.0.js');?>"></script>
	<script language="Javascript" type="text/javascript" src="<?php echo base_url('public/js/pagina-entrada/misc.js');?>"></script>
</head>
<body>
	<div id="wrapper">
		<div id="logo"></div>
		
		<div id="main">
			<div class="texto">Vem ai o lançamento do site da Fazenda Ouro Branco. Não percam!!!</div>
			<div id="countdown">				
				<div class="dash weeks_dash">
					<span class="dash_title">Semanas</span>
					<div class="digit">0</div>
					<div class="digit">0</div>
				</div>

				<div class="dash days_dash">
					<span class="dash_title">Dias</span>
					<div class="digit">0</div>
					<div class="digit">0</div>
				</div>

				<div class="dash hours_dash">
					<span class="dash_title">Horas</span>
					<div class="digit">0</div>
					<div class="digit">0</div>
				</div>

				<div class="dash minutes_dash">
					<span class="dash_title">Minutos</span>
					<div class="digit">0</div>
					<div class="digit">0</div>
				</div>

				<div class="dash seconds_dash">
					<span class="dash_title">Segundos</span>
					<div class="digit">0</div>
					<div class="digit">0</div>
				</div>				
			</div>
		</div>

		<script language="javascript" type="text/javascript">
			$(document).ready(function() {
				$('#countdown').countDown({
					targetDate: {
						'day': 		23,
						'month': 	8,
						'year': 	2015,
						'hour': 	00,
						'min': 		00,
						'sec': 		00
					},
				});		
			});
		</script>
	</div>
</body>
</html>