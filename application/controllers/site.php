<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {
	private $header = array();

	public function _remap($method){

		if(isset($_COOKIE['fundo-home'])){
			$this->header['fundo'] = $_COOKIE['fundo-home'];
		}

		if(PAGINA_ENTRADA){
			$this->pagina_entrada();
		}else if(method_exists("Site", $method)){
			$this->$method();
		}else{
			 $this->index();
		}
	}

	public function pagina_entrada()
	{
		$this->load->view('site/pagina-entrada');
	}

	public function index()
	{
		$this->header['menu_ativo'] = "home";

		$this->load->view('layout/header',$this->header);
		$this->load->view('site/index');
		$this->load->view('layout/footer');
	}

	public function muda_fundo()
	{
		if($this->input->server('REQUEST_METHOD') == 'POST'){
			if($this->input->post('tipo') == "dia"){
				$this->input->set_cookie("fundo-home","#57BDE9",time()+3600);

				echo json_encode(array(
					"fundo" => "#57BDE9"
				));
				exit;
			}else{
				$this->input->set_cookie("fundo-home","#003366",time()+3600);

				echo json_encode(array(
					"fundo" => "#003366"
				));
				exit;				
			}
		}else{
			$this->index;
		}
	}

	public function fazenda()
	{
		$this->header['menu_ativo'] = "a-fazenda";

		$this->load->view('layout/header',$this->header);
		$this->load->view('site/a-fazenda');
		$this->load->view('layout/footer');		
	}

	public function criacao_de_gados()
	{
		$this->header['menu_ativo'] = "criacao-de-gados";

		$this->load->view('layout/header',$this->header);		
		$this->load->view('site/criacao-de-gados');
		$this->load->view('layout/footer');		
	}

	public function gados()
	{
		$this->header['menu_ativo'] = "gados";

		$this->load->view('layout/header',$this->header);		
		$this->load->view('site/gados');
		$this->load->view('layout/footer');		
	}

	public function cadastro()
	{
		$this->header['menu_ativo'] = "cadastro";

		$this->load->view('layout/header',$this->header);		
		$this->load->view('site/cadastro');
		$this->load->view('layout/footer');		
	}			

	public function contato()
	{
		$this->header['menu_ativo'] = "contato";

		$this->load->view('layout/header',$this->header);		
		$this->load->view('site/contato');
		$this->load->view('layout/footer');		
	}		

}