<?php
Class Menu_model extends CI_Model
{
	public function CarregarMenu()
	{
		if($this->session->userdata('logged_in') == true){

			$sql = $this->db->query("SELECT * FROM admin_permissao_perfil WHERE id_perfil = ? ORDER BY ordem ASC",array($this->session->userdata('id_perfil')));

			if($sql->num_rows() > 0){
				return $sql->result_array();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
}
?>