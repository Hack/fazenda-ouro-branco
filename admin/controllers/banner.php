<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Banner extends CI_Controller {

	function __construct()
	{
    	parent::__construct();
  	}

    public function _remap($method)
    {
        if($this->session->userdata('logged_in')){        
            $this->$method();
        }else{
            redirect('login', 'refresh');
        }      
    }    

    public function index()
    {
        $header['titulo'] = "Banner";
        $header['breadcrumb'] = array(
            array(
                "titulo" => "Banner",
                "controller" => $this->router->fetch_class(),
                "action" => ""
            )          
        );

        $this->load->model('Banner_model');
        $data['banner'] = $this->Banner_model->Listar();

        $this->include_js = array('jquery.dataTables.min.js');
        $this->load->view('layout/header',$header);
        $this->load->view('banner/index',$data);
        $this->load->view('layout/footer');
    }

    public function editar()
    {
        $header['titulo'] = "Editar Banner";
        $header['breadcrumb'] = array(
            array(
                "titulo" => "Banner",
                "controller" => $this->router->fetch_class(),
            ),
            array(
                "titulo" => "Editar",
                "controller" => $this->router->fetch_class(),
            )            
        );

        $this->load->view('layout/header',$header);
        $this->load->view('banner/editar');
        $this->load->view('layout/footer');
    }

    public function novo()
    {
        $header['titulo'] = "Novo Banner";
        $header['breadcrumb'] = array(
            array(
                "titulo" => "Banner",
                "controller" => $this->router->fetch_class(),
            ),
            array(
                "titulo" => "Novo",
                "controller" => $this->router->fetch_class(),
            )            
        );

        $this->load->view('layout/header',$header);
        $this->load->view('banner/novo');
        $this->load->view('layout/footer');
    }    
}

?>