<div class="leftpanel">
	
    <div class="logopanel">
    	<h1><a href="<?php echo base_url('dashboard');?>">Toth Cursos</a></h1>
    </div>
    
    <div class="datewidget">Olá, <?php echo $this->session->userdata('nome');?>!</div>

	<div class="searchwidget">
    	<form action="results.html" method="post">
        	<div class="input-append">
                <input type="text" class="span2 search-query" placeholder="Busca">
                <button type="submit" class="btn"><span class="icon-search"></span></button>
            </div>
        </form>
    </div><!--searchwidget-->
    
    <div class="plainwidget">
    	<small>Usado 16.8 GB de 51.7 GB </small>
    	<div class="progress progress-info">
            <div class="bar" style="width: 20%"></div>
        </div>
        <small><strong>38% total</strong></small>
    </div><!--plainwidget-->

    <div class="leftmenu">        
        <ul class="nav nav-tabs nav-stacked">
			<?php
				if($this->uri->segment(1) == 'dashboard'){
					$class = "class='active'";
				}else{
					$class="";
				}
			?>            
            <li <?php echo $class;?>>
            	<a href="<?php echo base_url('dashboard');?>">
            		<span class="icon-home"></span> Dashboard
        		</a>
    		</li>

			<?php
				$menu = $this->Menu_model->CarregarMenu();

				if(!empty($menu)){
					foreach($menu as $value){
						if($value['controller'] == $this->uri->segment(1)){
							$class = "class='active'";
						}else{
							$class="";
						}						
			?>
			<li <?php echo $class;?>>
				<a href="<?php echo base_url("{$value['controller']}/");?>">
					<span class="<?php echo $value['icone'];?>"></span> <?php echo $value['descricao'];?>
				</a>
			</li>
			<?php
					}
				}
			?>        		
        </ul>
    </div><!--leftmenu-->
    
</div>