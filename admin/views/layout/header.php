<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" href="<?php echo base_url('public/img/favicon.ico');?>" type="image/x-icon"/>
    <title>Fazenda Ouro Branco</title>
    <link rel="stylesheet" href="<?php echo base_url('public/admin/css/style.default.css');?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url('public/admin/css/fazenda-ouro-branco.css');?>" type="text/css" />
    <script type="text/javascript">var base_url = "<?php echo base_url();?>";</script>
    <script type="text/javascript" src="<?php echo base_url('public/admin/js/jquery-1.9.1.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/admin/js/jquery-ui-1.9.2.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/admin/js/bootstrap.min.js');?>"></script>
</head>
<body>

<div class="mainwrapper fullwrapper">
	
    <?php $this->load->view("layout/menu");?>
    
    <!-- START OF RIGHT PANEL -->
    <div class="rightpanel">
    	<div class="headerpanel">
            
            <div class="headerright">
                
    			<div class="dropdown userinfo">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="<?php echo base_url('dashboard');?>">
                    	Opções <b class="caret"></b>
                	</a>
                    <ul class="dropdown-menu">
                        <li>
                        	<a href="editprofile.html">
                        		<span class="icon-edit"></span> Perfil
                    		</a>
                		</li>
                        <li class="divider"></li>
                        <li>
                        	<a href="<?php echo base_url('login/logout');?>">
                        		<span class="icon-off"></span> Sair
                    		</a>
                		</li>
                    </ul>
                </div>    		
            </div>            
    	</div>
        <div class="breadcrumbwidget">
        	<ul class="breadcrumb">
                <?php
                if(!empty($breadcrumb)){
                ?>
                <li>
                    <a href="<?php echo base_url('dashboard');?>">Dashboard</a>
                    <span class="divider">/</span>
                </li>
                <?php
                    foreach($breadcrumb as $key => $menu){
                        if(isset($breadcrumb[$key+1])){
                ?>
                <li>
                    <a href="<?php echo base_url("{$menu['controller']}/{$menu['action']}");?>"><?php echo $menu['titulo'];?></a>
                    <span class="divider">/</span>
                </li>
                <?php
                        }else{
                ?>
                <li class="active">
                    <?php echo $menu['titulo'];?>
                </li>
                <?php
                        }
                    }
                }else{
                ?>
                    <li class="active">
                        Dashboard
                    </li>
                <?php
                }
                ?>
            </ul>
        </div>
      	<div class="pagetitle">
        	<h1><?php echo $titulo;?></h1>
        </div>
        
        <div class="maincontent">
            <div class="contentinner">