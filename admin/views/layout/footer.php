			</div>
        </div>        
    </div>

    <div class="clearfix"></div>
    
    <div class="footer">

		<?php
			if( isset($this->include_js) ) {
				foreach( $this->include_js as $include_js ) {
		?>
					<script src="<?php echo base_url("public/admin/js/{$include_js}"); ?>"></script>
		<?php
				}
			}
		?>    
	    <script type="text/javascript" src="<?php echo base_url('public/admin/js/fazenda-ouro-branco.js');?>"></script>       
    	<div class="footerright">&copy; Toth Cursos Online <?php echo date("Y");?></div>
    </div>    
</div>

</body>
</html>