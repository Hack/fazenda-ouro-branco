<script type="text/javascript">
	$(document).ready(function(){
		Banner.Tabela();				
	});
</script>

<header class="header-right">
	<a href="<?php echo base_url("banner/novo");?>" class="btn btn-success btn-rounded">
		<i class="icon-plus icon-white"></i> Novo Banner
	</a>
</header>

<table class="table table-bordered" id="tabela-banner">
    <colgroup>
        <col class="con0" style="align: center; width: 4%" />
        <col class="con1" />
        <col class="con0" />
        <col class="con1" />
        <col class="con0" />
        <col class="con1" />
    </colgroup>
    <thead>
        <tr>
          	<th class="center"></th>
            <th class="center">Título</th>
            <th class="center">Descrição</th>
            <th class="center">Link</th>
            <th class="center">Data Cadastro</th>
            <th class="center acao-parcial">Ação</th>
        </tr>
    </thead>
    <tbody>
    <?php
    if($banner){
    	foreach($banner as $value){
    ?>
    <tr>
    	<td class="center v-center">
    		<img src="<?php echo base_url("public/img/sliderimages/{$value['imagem']}");?>" class="banner">
    	</td>
    	<td class="center v-center"><?php echo $value['titulo'];?></td>
    	<td class="center v-center"><?php echo $value['descricao'];?></td>
    	<td class="center v-center"><?php echo $value['link'];?></td>
    	<td class="center v-center"><?php echo date("d/m/Y",strtotime($value['data_cadastro']));?></td>
    	<td class="center v-center">
    		<a href="<?php echo base_url("banner/editar/{$value['id_banner']}");?>" class="btn btn-info btn-circle" title="Editar"><i class="icon-pencil icon-white"></i></a>
    		<a href="#" class="btn btn-danger btn-circle" title="Deletar"><i class="icon-trash icon-white"></i></a>
    	</td>
    </tr>
    <?php
    	}
    }
    ?>
    </tbody>
</table>