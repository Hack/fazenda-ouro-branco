<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="shortcut icon" href="<?php echo base_url('public/img/favicon.ico');?>" type="image/x-icon"/>
	<title>Fazenda Ouro Branco</title>
	<link rel="stylesheet" href="<?php echo base_url('public/admin/css/style.default.css');?>" type="text/css" />
	<script type="text/javascript" src="<?php echo base_url('public/admin/js/jquery-1.9.1.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('public/admin/js/login.js');?>"></script>
</head>

<body class="loginbody">
	<div class="loginwrapper">
		<img src="<?php echo base_url("public/img/logo.png");?>" width="300" height="100" class="logo">
		<div class="loginwrap zindex100 animate2 bounceInDown">
		<h1 class="logintitle"><span class="iconfa-lock"></span> Login <span class="subtitle">Seja bem-vindo!</span></h1>
	        <div class="loginwrapperinner">
	            <form id="loginform" action="<?php echo base_url("login/");?>" method="POST" onsubmit="return Login.Logar();">
	                <p><input type="text" id="usuario" name="usuario" placeholder="Usuário"></p>
	                <p><input type="password" id="senha" name="senha" placeholder="Senha"></p>
	                <p><button class="btn btn-default btn-block">Entrar</button></p>
	                <p class="mensagem-erro">
			        	<?php
			        	if(isset($erro_validacao) && !empty($erro_validacao)){
			        		echo $erro_validacao;
			        	}
			        	?>	                	
	                </p>
	                <p><a href="<?php echo base_url();?>"><span class="icon-question-sign icon-white"></span> Esqueci a senha?</a></p>
	            </form>
	        </div>
	    </div>
	    <div class="loginshadow animate4 fadeInUp"></div>
	</div>
</body>
</html>
