var Geral = {

	FundoHome: function(tipo){
		$.ajax({
	  		url: base_url+"site/muda_fundo",
		  	method: "POST",
		  	data: {
	  			tipo: tipo 
	  		},
	  		dataType: "JSON",
	  		success:function(data){

				$("body").animate({
					backgroundColor: data.fundo
				}, 500);
	  		}		  	
		});
	}
}

var Home = {

	init: function(){
	    $('.jcarousel').jcarousel();

	    $('.jcarousel-control-prev')
	        .on('jcarouselcontrol:active', function() {
	            $(this).removeClass('inactive');
	        })
	        .on('jcarouselcontrol:inactive', function() {
	            $(this).addClass('inactive');
	        })
	        .jcarouselControl({
	            target: '-=1'
	        });

	    $('.jcarousel-control-next')
	        .on('jcarouselcontrol:active', function() {
	            $(this).removeClass('inactive');
	        })
	        .on('jcarouselcontrol:inactive', function() {
	            $(this).addClass('inactive');
	        })
	        .jcarouselControl({
	            target: '+=1'
	        });

	    $('.jcarousel-pagination')
	        .on('jcarouselpagination:active', 'a', function() {
	            $(this).addClass('active');
	        })
	        .on('jcarouselpagination:inactive', 'a', function() {
	            $(this).removeClass('active');
	        })
	        .jcarouselPagination();		
	}

};

var Criacao = {

	init: function(){
		var icons = {
			header: "ui-icon-help",
			activeHeader: "ui-icon-check"
		};

		$("#accordion").accordion({
			collapsible: true,
			active: false,
			icons: icons,
		});		
	}

};